import * as puppeteer from 'puppeteer';

describe('Main page', () => {
  beforeAll(async () => {
    await page.goto("http://localhost:8080");
  });

  it('should be titled "Google"', async () => {
    await expect(page.title()).resolves.toMatch('shopping-cart');
  });
});
